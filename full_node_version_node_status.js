Drupal.behaviors.full_node_version_node_status_form = function(c) {
  $('#full-node-version-node-status-form input[value="disabled"]').attr('disabled','disabled');
  $("#full-node-version-node-status-form .form-checkboxes .form-item input").click( function() {
    checking = $(this).is(":checked");
    if (checking) {
      $(this).parents(".form-checkboxes").find("input").not(this).attr('checked', false);
      if($(this).attr('id') == 'edit-live-status-d') {
       $('#edit-testing-status-d').attr('checked','TRUE'); 
       $('#edit-testing-status-c').removeAttr('checked'); 
      }
    }
    else {
    }
  
  });
};
